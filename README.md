# From metagenomes to metabolism: Systematically assessing the metabolic flux feasibilities for “Candidatus Accumulibacter” species during anaerobic substrate uptake
Timothy Páez-Watson, Mark C.M. van Loosdrecht, S. Aljoscha Wahl.
***

This project includes all the information and codes used for the simulations of the article.

## Table of Contents
- [Project Description](#project-description)
- [Simulation Folders](#simulation-folders)
- [Getting Started](#getting-started)

## Project Description

The article provides a pipeline to combine genetic analysis, Elementary Flux Mode Analysis (EFMA) and Max-Min Driving Force (MDF) calculations to systematically assess metabolic models. The main application of this pipeline is for metagenome assembled genomes (MAGs) from microbial communities. 

This project specifically contains the source code and data that was used to simulate and generate the content for the article. Users that wish to apply a similar pipeline to their metagenomic data can download the content of this folder to use as a starting point. 


## Simulation Folders

This repository contains the following simulation folders:

- **1_Genetic Analysis**: Analysis on presence or abscence of specific genes from the metabolic model in MAGs of "Candidatus Accumulibacter”. 
- **2_FBA**: EFMA computation from a given metabolic model, data processing and plotting. 
- **3_Thermodynamics**: MDF calculations on each EFMA, data processing and plotting. 

Each folder may contain simulation scripts, data files, or other relevant materials. Below are brief descriptions of the folders:

- **1_Genetic Analysis**:
  - Main files: Jupyter notebook files (.ipynb) "1_Blast_anaplerotic on all genomes" and "2_Data visualization". 
  - Contents: Data required to perform blast and data visualization on a selected number of MAGs. Note, only 2 MAGs have been uploaded due to space constraints.

- **2_FBA**:
  - Main files: Jupyter notebook files (.ipynb) "1_Model_check", "2_Elementary_Flux_Modes filtering" and "3_Plotting_EFMs_Yields".
  - Contents: Model structure, efm tool (Matlab) and scripts (Python) used to process data data and plot.


- **3_Thermodynamics**:
  - Main files: Jupyter notebook files (.ipynb) "1_MDF calculation of all models with Equilibrator", "2_Plots of models based on MDF", "3_Yields distribution on most feasible models".
  - Contents: Model structure and scripts to perform MDF analysis on all EFMs. Additional scripts to process data and generate plots.


## Getting Started

Download the content of this project and unpack the zip code. The scripts (main files) from each folder can be open with Jupyter notebooks (Python). The following versions of packages were used:
- pandas 1.4.4
- biopython 0.4.4
- numpy 1.21.5
- scipy 1.9.1
- matplotlib 3.5.2
- equilibrator_pathway 0.4.7

